package com.gitlab.ccook.acps;

import com.gitlab.ccook.acps.model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

public class ACPSAnalyzer {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            displayHelp();
        }
        String filePath = args[0];
        List<Incident> incidents = readIncidents(filePath);
        analyzeIncidents(incidents);

    }

    private static void analyzeIncidents(List<Incident> incidents) throws Exception {
        analyzeTotal(0, "ACPS-WIDE", incidents);
        for (School s : School.values()) {
            if (!s.equals(School.UNKNOWN)) {
                List<Incident> bySchool = new ArrayList<>();
                for (Incident i : incidents) {
                    if (i.getSchool().equals(s)) {
                        bySchool.add(i);
                    }
                }
                analyzeSchool(0, s, bySchool);
            }
        }
    }

    private static void analyzeSchool(int tabLevel, School s, List<Incident> incidents) {
        printWithTabs(tabLevel, "===== SCHOOL: " + s + " =========");
        analyzeTotal(tabLevel, s + "", incidents);
    }

    private static void analyzeTotal(int tabLevel, String header, List<Incident> incidents) {
        printWithTabs(tabLevel, "====== " + header.toUpperCase() + " OVERALL ===========");
        printWithTabs(tabLevel, "");
        printWithTabs(tabLevel, "Total Incidents: " + incidents.size());
        printWithTabs(tabLevel, "");
        analyzeByCategory(tabLevel + 1, header, incidents);
        analyzeByGrade(tabLevel + 1, header, incidents);
        analyzeByRace(tabLevel + 1, header, incidents);
        analyzeByLatinx(tabLevel + 1, header, incidents);
    }

    private static void analyzeByLatinx(int tabLevel, String header, List<Incident> incidents) {
        Map<Boolean, List<Incident>> byLatinx = new HashMap<>();
        for (Incident i : incidents) {
            List<Incident> latinXIncident = new ArrayList<>();
            if (byLatinx.containsKey(i.getHispanic())) {
                byLatinx.get(i.getHispanic()).add(i);
            } else {
                latinXIncident.add(i);
                byLatinx.put(i.getHispanic(), latinXIncident);
            }
        }
        printWithTabs(tabLevel, "====== BY HISPANIC/LATINX ===========");
        for (Map.Entry<Boolean, List<Incident>> integerListEntry : byLatinx.entrySet()) {
            printWithTabs(tabLevel, "HISPANIC/LATINX: " + integerListEntry.getKey());
            analyzeByCategory(tabLevel + 1, header + "-> Hispanic (" + integerListEntry.getKey() + ")", integerListEntry.getValue());
        }
    }

    private static void analyzeByRace(int tabLevel, String header, List<Incident> incidents) {
        Map<String, List<Incident>> byRace = new HashMap<>();
        for (Incident i : incidents) {
            List<Incident> raceIndicdent = new ArrayList<>();
            if (byRace.containsKey(i.getRaceCode())) {
                byRace.get(i.getRaceCode()).add(i);
            } else {
                raceIndicdent.add(i);
                byRace.put(i.getRaceCode(), raceIndicdent);
            }
        }
        printWithTabs(tabLevel, "====== BY RACE ===========");
        for (Map.Entry<String, List<Incident>> integerListEntry : byRace.entrySet()) {
            printWithTabs(tabLevel, "Race: " + integerListEntry.getKey());
            analyzeByCategory(tabLevel + 1, header + "-> Race(" + integerListEntry.getKey() + ")", integerListEntry.getValue());
        }
    }

    private static void analyzeByGrade(int tabLevel, String header, List<Incident> incidents) {
        Map<Integer, List<Incident>> byGrade = new HashMap<>();
        for (Incident i : incidents) {
            List<Incident> gradeIncident = new ArrayList<>();
            if (byGrade.containsKey(i.getGrade())) {
                byGrade.get(i.getGrade()).add(i);
            } else {
                gradeIncident.add(i);
                byGrade.put(i.getGrade(), gradeIncident);
            }
        }
        printWithTabs(tabLevel, "====== BY GRADE ===========");
        for (Map.Entry<Integer, List<Incident>> integerListEntry : byGrade.entrySet()) {
            printWithTabs(tabLevel, "Grade: " + integerListEntry.getKey());
            analyzeByCategory(tabLevel + 1, header + "-> " + integerListEntry.getKey(), integerListEntry.getValue());
        }
    }

    private static void analyzeByCategory(int tabLevel, String header, List<Incident> incidents) {
        printWithTabs(tabLevel, "====== " + header.toUpperCase() + " BY OFFENSE CATEGORY ===========");
        printWithTabs(tabLevel, "");
        Map<OffenseCategory, List<Incident>> byCategory = new HashMap<>();
        for (Incident i : incidents) {
            OffenseCategory category = i.getPrimaryOffenseDescription().getCategory();
            List<Incident> categoryIncidents = new ArrayList<>();
            if (byCategory.containsKey(category)) {
                byCategory.get(category).add(i);
            } else {
                categoryIncidents.add(i);
                byCategory.put(category, categoryIncidents);
            }
        }

        for (Map.Entry<OffenseCategory, List<Incident>> ent : byCategory.entrySet()) {
            printWithTabs(tabLevel, "Category: " + ent.getKey() + " | " + "Total: " + ent.getValue().size() + " | " + "% of " + header + ": " + toPercentage(((double) ent.getValue().size() / incidents.size())));
            analyzeSanction(tabLevel + 1, header + "-> " + ent.getKey(), ent.getValue());
            printWithTabs(tabLevel, "");
            Map<Offense, List<Incident>> byOffense = new HashMap<>();
            for (Incident i : ent.getValue()) {
                List<Incident> offenseIncidents = new ArrayList<>();
                if (byOffense.containsKey(i.getPrimaryOffenseDescription())) {
                    byOffense.get(i.getPrimaryOffenseDescription()).add(i);
                } else {
                    offenseIncidents.add(i);
                    byOffense.put(i.getPrimaryOffenseDescription(), offenseIncidents);
                }
            }
            for (Map.Entry<Offense, List<Incident>> offenseListEntry : byOffense.entrySet()) {
                printWithTabs(tabLevel + 1, "Offense: " + offenseListEntry.getKey() + " | " + "Total: " + offenseListEntry.getValue().size() + " | % of " + header + "-> " + offenseListEntry.getKey().getCategory() + " " + toPercentage(((double) offenseListEntry.getValue().size() / ent.getValue().size())));
                analyzeSanction(tabLevel + 1, header + "-> " + offenseListEntry.getKey().getCategory() + "-> " + offenseListEntry.getKey(), offenseListEntry.getValue());
            }
            printWithTabs(tabLevel, "");
        }
    }

    private static void analyzeSanction(int tabLevel, String header, List<Incident> incidents) {
        Map<Sanction, List<Incident>> sanctionListMap = new HashMap<>();
        for (Incident i : incidents) {
            List<Incident> incidentList = new ArrayList<>();
            if (sanctionListMap.containsKey(i.getSanction())) {
                sanctionListMap.get(i.getSanction()).add(i);
            } else {
                incidentList.add(i);
                sanctionListMap.put(i.getSanction(), incidentList);
            }
        }
        for (Map.Entry<Sanction, List<Incident>> ent : sanctionListMap.entrySet()) {
            printWithTabs(tabLevel + 1, "Sanction: " + ent.getKey() + " | Total : " + ent.getValue().size() + " |  % of " + header + " : " + toPercentage((double) ent.getValue().size() / incidents.size()));
        }

    }

    private static void printWithTabs(int tabLevel, String msg) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tabLevel; i++) {
            sb.append("\t");
        }
        System.out.println(sb.append(msg));
    }

    private static String toPercentage(double v) {
        String asString = (v * 100) + "";
        String percentage = asString.split("\\.")[0] + "%";
        return percentage;
    }

    private static List<Incident> readIncidents(String filePath) throws Exception {

        FileInputStream excelFile = new FileInputStream(new File(filePath));
        Workbook workbook = new HSSFWorkbook(excelFile);
        Sheet datatypeSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = datatypeSheet.iterator();
        boolean firstCell = true;
        List<Incident> incidents = new ArrayList<>();
        while (iterator.hasNext()) {
            Row currentRow = iterator.next();
            if (!firstCell) {

                String school = currentRow.getCell(0).getStringCellValue().trim();
                if (!school.isEmpty()) {
                    int gradeLevel = (int) Double.parseDouble(currentRow.getCell(1).toString());
                    boolean hispanic = currentRow.getCell(2).getStringCellValue().trim().equalsIgnoreCase("Y");
                    String raceCode = currentRow.getCell(3).getStringCellValue();
                    Gender gender = Gender.getGenderFromCode(currentRow.getCell(4).getStringCellValue());
                    String primaryOffenseDesc = currentRow.getCell(5).getStringCellValue();
                    String finalSaction = currentRow.getCell(6).getStringCellValue();
                    incidents.add(new Incident(School.getSchoolFromID(school), gradeLevel, hispanic, raceCode, gender, Offense.getOffenseFromName(primaryOffenseDesc), Sanction.getSanctionFromCode(finalSaction), UUID.randomUUID()));

                }
            }
            firstCell = false;
        }
        return incidents;
    }

    public static void displayHelp() {
        printWithTabs(0, "Usage: java -jar <jar file> <excel spreadsheet>");
        System.exit(0);
    }
}
