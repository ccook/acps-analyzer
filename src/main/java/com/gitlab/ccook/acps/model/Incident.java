package com.gitlab.ccook.acps.model;

import java.util.Objects;
import java.util.UUID;

public class Incident {
    private School school;
    private int grade;
    private boolean hispanic;
    private String raceCode;
    private Gender gender;
    private Offense primaryOffenseDescription;
    private Sanction sanction;
    private UUID id;

    public Incident(School sch, int grade, boolean hispanic, String raceCode, Gender g, Offense o, Sanction sanction, UUID id) {
        this.school = sch;
        this.grade = grade;
        this.hispanic = hispanic;
        this.raceCode = raceCode;
        this.gender = g;
        this.primaryOffenseDescription = o;
        this.sanction = sanction;
        this.id = id;
    }

    public School getSchool() {
        return school;
    }

    public int getGrade() {
        return grade;
    }

    public boolean isHispanic() {
        return hispanic;
    }

    public String getRaceCode() {
        return raceCode;
    }

    public Gender getGender() {
        return gender;
    }

    public Offense getPrimaryOffenseDescription() {
        return primaryOffenseDescription;
    }

    public Sanction getSanction() {
        return sanction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Incident incident = (Incident) o;
        return grade == incident.grade &&
                hispanic == incident.hispanic &&
                school == incident.school &&
                Objects.equals(raceCode, incident.raceCode) &&
                gender == incident.gender &&
                Objects.equals(primaryOffenseDescription, incident.primaryOffenseDescription) &&
                sanction == incident.sanction &&
                Objects.equals(id, incident.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(school, grade, hispanic, raceCode, gender, primaryOffenseDescription, sanction, id);
    }

    @Override
    public String toString() {
        return "Incident{" +
                "school=" + school +
                ", grade=" + grade +
                ", hispanic=" + hispanic +
                ", raceCode='" + raceCode + '\'' +
                ", gender=" + gender +
                ", primaryOffenseDescription='" + primaryOffenseDescription + '\'' +
                ", sanction=" + sanction +
                ", id=" + id +
                '}';
    }

    public Boolean getHispanic() {
        return hispanic;
    }
}
