package com.gitlab.ccook.acps.model;

public enum Gender {
    MALE("M"),

    FEMALE("F"),

    UNKNOWN("");

    private String code;

    Gender(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }

    public static Gender getGenderFromCode(String code) {
        for (Gender g : Gender.values()) {
            if (g.code.equalsIgnoreCase(code)) {
                return g;
            }
        }

        return UNKNOWN;
    }
}
