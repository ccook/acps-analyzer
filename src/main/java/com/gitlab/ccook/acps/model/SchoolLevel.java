package com.gitlab.ccook.acps.model;

public enum SchoolLevel {
    ELEMENTARY,
    MIDDLE,
    HIGH;
}
