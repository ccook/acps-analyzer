package com.gitlab.ccook.acps.model;

public enum Sanction {
    IN_SCHOOL_SUSPENSION("01"),
    SHORT_TERM_SUSPENSION("02"),
    LONG_TERM_SUSPENSION("03"),
    UNKNOWN("");

    private String code;

    Sanction(String code) {
        this.code = code;
    }

    public static Sanction getSanctionFromCode(String code) {
        for (Sanction s : Sanction.values()) {
            if (s.code.equalsIgnoreCase(code)) {
                return s;
            }
        }
        return UNKNOWN;
    }
}
