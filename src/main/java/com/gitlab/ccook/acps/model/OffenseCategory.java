package com.gitlab.ccook.acps.model;

public enum OffenseCategory {
    MISUSE_OF_PROPERTY,
    VAGUE,
    DRUGS,
    WEAPON_POSSESSION,
    THEFT,
    VIOLENCE,
    UNKNOWN;
}
