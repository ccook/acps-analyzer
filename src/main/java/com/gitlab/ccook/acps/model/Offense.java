package com.gitlab.ccook.acps.model;

public enum Offense {
    VIOLATE_AUP("Violate AUP", OffenseCategory.MISUSE_OF_PROPERTY),
    CELL_PHONES("Cellular Phones", OffenseCategory.MISUSE_OF_PROPERTY),
    FIRE_ALARM("False fire alarm", OffenseCategory.MISUSE_OF_PROPERTY),
    UNAUTH_USE_OF_TECH("Unauth Use of Tech.", OffenseCategory.MISUSE_OF_PROPERTY),

    MISREPRESENTATION("Misrepresentation", OffenseCategory.VAGUE),
    DISREPSECT("Disrespect", OffenseCategory.VAGUE),
    INSUBORDINATION("Insubordination", OffenseCategory.VAGUE),
    DEFIANCE("Defiance", OffenseCategory.VAGUE),
    DISRUPTIVE_DEMO("Disruptive Demo", OffenseCategory.VAGUE),
    CLASSROOM_DISRUPTION("Classroom Disruption", OffenseCategory.VAGUE),
    OBSCENE_LANGUAGE_GESTURE("Obscene Lang/Gesture", OffenseCategory.VAGUE),

    POSSESS_OTC_MEDS("Possess OTC Meds", OffenseCategory.DRUGS),
    ELECTRONIC_CIGARETTE("Electronic cigarette", OffenseCategory.DRUGS),
    DRUG_PARAPHENALIA("Drug Paraphenalia", OffenseCategory.DRUGS),
    TOBACCO_PRODUCTS("Tobacco Products", OffenseCategory.DRUGS),
    POSSESS_INHALENTS("Possess inhalents", OffenseCategory.DRUGS),
    POSSESS_SCH_1_2("Possess/Use Sch I/II", OffenseCategory.DRUGS),
    ALCOHOL("Alcohol_AC1_AC2_AC30", OffenseCategory.DRUGS),
    OTC_MED_USE("OTC Medicine Use", OffenseCategory.DRUGS),
    SALE_DIST_SCH_1_2("Sale/Dist Sch I/II", OffenseCategory.DRUGS),

    POSSESS_RAZOR("Possess Razor, etc.", OffenseCategory.WEAPON_POSSESSION),
    POSSESS_BB_GUN("Possess BB Gun", OffenseCategory.WEAPON_POSSESSION),
    POSSESS_KNIFE("Possess Knife >3 in.", OffenseCategory.WEAPON_POSSESSION),

    THEFT_SF_PROPERTY("(Pre1819) TheftSfPrp", OffenseCategory.THEFT),
    THEFT_ST_PROPERTY("(Pre1819) TheftStPrp", OffenseCategory.THEFT),
    THEFT_SC_PROPERTY("(Pre1819) TheftScPrp", OffenseCategory.THEFT),

    BATTERY_STUDENT_WEAPON("Battery/Student/Wpn", OffenseCategory.VIOLENCE),
    CYBER_BULLYING("Cyber Bullying", OffenseCategory.VIOLENCE),
    BATTERY_STUDENT("Battery/Student", OffenseCategory.VIOLENCE),
    SEXUAL_TOUCH_STUDENT("Sexual Touch/Student", OffenseCategory.VIOLENCE),
    FIGHT_NO_INJURY("Fight w/o injury", OffenseCategory.VIOLENCE),
    HARASSMENT("Harassment", OffenseCategory.VIOLENCE),
    THREAT_OF_STAFF("Threat of Staff", OffenseCategory.VIOLENCE),
    GANG_ACTIVITY("Gang Activity", OffenseCategory.VIOLENCE),
    BATTERY_STAFF("Battery/Staff", OffenseCategory.VIOLENCE),
    BATTERY_NO_INJURY("Battery no injury", OffenseCategory.VIOLENCE),
    THREAT_OF_STUDENT("Threat of Student", OffenseCategory.VIOLENCE),
    BULLYING("Bullying", OffenseCategory.VIOLENCE),
    ALTERCATION("Altercation", OffenseCategory.VIOLENCE),
    SEXUAL_HARRASSMENT("Sexual Harrassment", OffenseCategory.VIOLENCE),
    OTHER("Other School Viol.", OffenseCategory.UNKNOWN);;


    private String name;
    private OffenseCategory category;

    Offense(String name, OffenseCategory category) {
        this.name = name;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public OffenseCategory getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return name;
    }

    public static Offense getOffenseFromName(String name) {
        for (Offense o : Offense.values()) {
            if (o.name.equalsIgnoreCase(name)) {
                return o;
            }
        }
        return OTHER;
    }
}
