package com.gitlab.ccook.acps.model;

public enum School {
    Ferdinand("Ferdinand T. Day Elementary School (350)"),
    Jefferson_Houston("Jefferson-Houston School (90)"),
    Minnie_Howard("T.C. Williams - Minnie Howard Campus (40)"),
    Chance_for_Change("Chance for Change (500)"),
    Francis_C_Hammond("Francis C. Hammond Middle School (170)"),
    TC_Williams("T.C. Williams High School (210)"),
    GW("George Washington Middle School (10)"),
    UNKNOWN("");
    private String name;

    School(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return name;
    }

    public static School getSchoolFromID(String id) {
        for (School s : School.values()) {
            if (s.name.equalsIgnoreCase(id)) {
                return s;
            }
        }
        return School.UNKNOWN;
    }
}
